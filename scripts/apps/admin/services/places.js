'use strict';

angular.module('dc.admin')
    .factory('Admin', function($http) {

        var urlBase = '/api/admin';
        var adminFactory = {};

        adminFactory.addPlace = function(data) {
            return $http.post(urlBase + '/places', data);
        };

        return adminFactory;
    });
