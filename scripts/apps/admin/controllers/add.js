'use strict';

angular.module('dc.admin')
    .controller('AddCtrl', function ($scope, $window, Admin) {

        $scope.form = {};
        $scope.addPlace = function() {
            Admin.addPlace($scope.form)
                .success(function (response) {
                    if(response.error) {
                        $window.alert('Error - ' + response);
                    } else {
                        $window.alert('ok');
                    }
                });
        };

    });
