'use strict';

angular.module('dc.admin', []);

angular.element(document).ready(function() {
    angular.bootstrap(document, ['dc.admin']);
});
