'use strict';

angular.module('dc')
    .factory('Places', function($http) {

        var urlBase = '/api/places';
        var placesFactory = {};

        placesFactory.getPlaces = function(type) {
            if(type) {
                return $http.get(urlBase + '/' + type, {cache: true});
            } else {
                return $http.get(urlBase, {cache: true});
            }
        };

        placesFactory.addPlace = function(data) {
            return $http.post(urlBase, data);
        };

        return placesFactory;
    });
