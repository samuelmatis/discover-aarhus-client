'use strict';

angular.module('dc')
    .factory('Map', function() {

        var mapFactory = {};

        mapFactory.getConfig = function() {
            return {
                center: {
                    latitude: 56.1449797,
                    longitude: 10.1875297
                },
                options: {
                    streetViewControl: true,
                    panControl: false,
                    maxZoom: 20,
                    minZoom: 3
                },
                places: [],
                zoom: 14,
                scrollwheel: false
            };
        };

        return mapFactory;
    });
