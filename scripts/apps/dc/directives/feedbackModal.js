'use strict';

angular.module('dc')
    .directive('feedbackModal', function() {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: 'templates/feedback.tpl.html',
            controller: 'FeedbackCtrl'
        }
    });
