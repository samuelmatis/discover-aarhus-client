'use strict';

angular.module('dc')
    .directive('addPlaceModal', function() {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: 'templates/add.tpl.html',
            controller: 'AddCtrl'
        }
    });
