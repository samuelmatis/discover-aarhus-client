'use strict';

angular.module('dc')
    .directive('app', function() {
        return {
            restrict: 'E',
            controller: 'MainCtrl',
            templateUrl: 'templates/index.tpl.html',
            link: function(scope, element) {
                new cbpHorizontalSlideOutMenu(element[0].querySelector('#cbp-hsmenu-wrapper'));
            }
        };
    });
