'use strict';

angular.module('dc')
    .directive('trackActive', function($location) {
        return function (scope, element){
            scope.$watch(function() {
                return $location.path();
            }, function() {
                var links = element.find('li');
                links.removeClass('active');

                angular.forEach(links, function(value){
                    var el = angular.element(value),
                        link = el.children();

                    if (link.attr('href') === $location.path()) {
                        el.addClass('active');
                    }
                });
            });
        };
    });
