'use strict';

angular.module('dc', [
    'ngRoute',
    'google-maps',
    'ngProgressLite'
])
.config(function ($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/list', { controller: 'ListCtrl', templateUrl: 'templates/list.tpl.html' })
        .when('/:type?', { controller: 'MapCtrl', templateUrl: 'templates/map.tpl.html' })
        .otherwise({ redirectTo: '/' });
})
.run(function($rootScope, ngProgressLite) {
    $rootScope.$on('$routeChangeStart', function() {
        ngProgressLite.start();
    });

    $rootScope.$on('$routeChangeSuccess', function() {
        ngProgressLite.done();
    });
});

angular.element(document).ready(function() {
    angular.bootstrap(document, ['dc']);
});
