'use strict';

angular.module('dc')
    .controller('MainCtrl', function($scope) {

        $scope.addPlaceModal = function() {
            $scope.$broadcast('addPlaceModal');
        };

        $scope.sendFeedbackModal = function() {
            $scope.$broadcast('sendFeedbackModal');
        };

    });
