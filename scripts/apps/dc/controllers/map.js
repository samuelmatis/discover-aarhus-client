'use strict';

angular.module('dc')
    .controller('MapCtrl', function ($scope, $routeParams, Map, Places) {

        // Main map object with configuration
        $scope.map = Map.getConfig();

        // Fetch places
        Places.getPlaces($routeParams.type)
            .success(function (places) {
                $scope.map.places = places;
            });

    });
