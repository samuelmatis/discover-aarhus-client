'use strict';

angular.module('dc')
    .controller('AddCtrl', function ($scope, $window, Places) {

        $scope.modalShown = false;

        $scope.$on('addPlaceModal', function() {
            $scope.modalShown = !$scope.modalShown;
        });

        $scope.form = {};
        $scope.addPlace = function() {
            Places.addPlace($scope.form)
                .success(function (response) {
                    if(response.error) {
                        $window.alert('Error - ' + response);
                    } else {
                        $window.alert('ok');
                    }
                });
        };

    });
