'use strict';

angular.module('dc')
    .controller('ListCtrl', function ($scope, Places) {

        // Get places
        Places.getPlaces()
            .success(function (places) {
                $scope.places = places;
            });

    });
